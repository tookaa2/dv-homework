var result = 0,
    numbers = []

const sum = (numbers) => {

    for (let number of numbers) {
        result += number;
    }

    console.log(result);
    result = 0;
}

const range = (num1, num2, num3) => {
    let index = 0;
    if (num1 <= num2) {
        if (num3 === undefined) {
            for (let i = num1; i <= num2; i++) {
                numbers[index] = i;
                index++;
            }
        } else if (num3 !== undefined && num3 > 0) {
            for (let i = num1; i <= num2; i += num3) {
                numbers[index] = i;
                index++;
            }
        }
    } else if (num1 > num2) {
        if (num3 === undefined) {
            for (let i = num1; i >= num2; i--) {
                numbers[index] = i;
                index++;
            }
        } else if (num3 !== undefined && num3 <= 0) {
            for (let i = num1; i >= num2; i += num3) {
                numbers[index] = i;
                index++;
            }
        }
    }

    console.log("check numbers " + numbers);
    return sum(numbers)

}

// range(1, 10);
range(10, -1, -3);
range(-1, 10, -3);
range(2, 10);
range(10, -2);
// range(10, 1);

