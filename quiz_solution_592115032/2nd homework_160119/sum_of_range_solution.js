function range(num1,num2){
    let array = [];
    for(let i = num1; i <= num2; i++){
        array.push(i)
    }
    return array
}

function rangeStep(num1,num2,step){
    let array=[];
    for(let i = num1; i < num2; ){
        array.push(i);
        i += step;
    }
    return array;
}