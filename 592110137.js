// Triangle function
function makeLine (length){ // function for make line according to the number of triangle entered.
    let line = "";
    for(let j = 1; j <= length; j++){ // loop line
        line += "# ";
    }
    return line + "\n"; // print # with new line
}

function buildTriangle(x){ // function for make column according to the number of triangle entered.
    let triangle = '';

    for(let t = 1; t <= x; t++){ // loop colum
        triangle += makeLine(t); // call makeline function for make line
    }
    return triangle;
}
// call function and input the number of triangle from user
console.log(buildTriangle(Number(prompt("Enter the number of triangle"))));


//Fizzbuzz function
function Fizzbuzz (aTarget){
    let str=""; // this is a variable empty string for add 'Fizz' or 'Buzz' or newline
    let x,y;
    for (let i = 1; i <= 100; i++) // this loop increase i 1 to 100 for check each i
    {
        x = i%3 ==0; // this variable is condition Divide 3 perfectly
        y = i%5 ==0; // this variable is condition Divide 5 perfectly
        if(x)
        {
            str += "Fizz"; // keep string 'Fizz'
        }
        if (y)
        {
            str += "Buzz"; // keep string 'Fizz'
        }
        if (!(x||y)) // this condition for when not Divide 3 or 5 perfectly
        {
            str += i; // keep number for display
        }
        str += "\n"; // newline
    }
    console.log(str); // Display variable str
}
Fizzbuzz(100); // run the function and send the numeric value to the loop and display


//Chessboard function
function Chessboard(size){
    let board = ""; //this is a variable empty string for add ' ' or '#' or newline

    for (let i = 0; i < size; i++) { //this loop for add newline each rows
      for (let j = 0; j < size; j++) { // this loop for adding ' ' or '#' into board variable
        if ((i + j) % 2 == 0)
          board += " "; // keep string ' '
        else
          board += "#"; // keep string '#'
      }
      board += "\n"; // add newline
    }
    
    return board;
}
console.log(Chessboard(Number(prompt("Enter the size of Chessboard")))); // input the size from user